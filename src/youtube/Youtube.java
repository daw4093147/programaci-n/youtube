package youtube;

import java.util.ArrayList;
import java.util.Scanner;

public class Youtube {

	private static Scanner sc = new Scanner(System.in);
	private static ArrayList<Canal> canales = new ArrayList<Canal>();

	public static void main(String[] args) {
		mostrarMenu();
	}

	private static void crearCanal() {

		System.out.println("¿Cómo quieres llamar a tu canal?");
		String nombre = sc.nextLine();
		Canal nuevoCanal = new Canal(nombre);
		canales.add(nuevoCanal);
		System.out.println("");
		
		nuevoCanal.menuCanal();		
	}

	private static void seleccionarCanal() {
		Canal canal;
		if (canales.isEmpty()) {
			System.out.println("No hay canales disponibles. Crea un canal.");
			crearCanal();
		} else {
			System.out.println("Selecciona canal:");
			for (int i = 0; i < canales.size(); i++) {
				canal = canales.get(i);
				System.out.println(i + "-Nombre del canal: " + canal.getNombre() + " creado en fecha: "
						+ canal.getFecha() + " con " + canal.videos.size() + " videos");
			}
			
			int opcion = sc.nextInt();
			sc.nextLine();

			if (opcion >= 0 && opcion < canales.size()) {
				canal = canales.get(opcion);
				canal.menuCanal();
			} else {
				System.out.println("Opción no válida. Intentálo de nuevo.");
			}
		}
	}

	private static void mostrarEstadisticas() {
		if (canales.size() == 1) {
			System.out.println("Youtube tiene " + canales.size() + " canal");
		} else {
			 System.out.println("YouTube tiene " + canales.size() + " canales");
		}
		System.out.println("");
	}

	private static void mostrarEstadisticasCompletas() {
		System.out.println("Youtube tiene " + canales.size() + " canales");
		for (Canal canal : canales) {
			System.out.println("- Nombre del canal: " + canal.getNombre() + " creado en fecha: " + canal.getFecha() 
			+ " con " + canal.videos.size() + " vídeos." );
			for (Video video : canal.videos) {
				System.out.println("-- Video: " + video.getTitulo() + " en fecha: " + video.getFecha()
						+ " con " + video.getLike() + " likes y " + video.comentario.size() + " comentarios.");
				for (Comentario comentario : video.comentario) {
					System.out.println("--- Comentario: " + comentario.getComentario() + " del usuario " + comentario.getUsuario()
					+ " en fecha " + comentario.getFecha());
				}
			}
		}
		System.out.println("");
 	}

	private static void mostrarMenu() {
		while (true) {
			System.out.println("|---YOUTUBE---|");
			System.out.println("1- Nuevo canal");
			System.out.println("2- Seleccionar Canal");
			System.out.println("3- Mostrar estadísticas");
			System.out.println("4- Mostrar estadísticas completas");
			System.out.println("0- Salir");
			System.out.println("|-------------|");
			
			int opcion = sc.nextInt();
			sc.nextLine();
	
				switch (opcion) {
					case 0: 
						System.out.println("Saliendo del programa.");
						System.exit(0);
					case 1:
						crearCanal();
						break;
					case 2:
						seleccionarCanal();
						break;
					case 3:
						mostrarEstadisticas();
						break;
					case 4:
						mostrarEstadisticasCompletas();
						break;
					default:
						System.out.println("Opción no válida. Inténtalo de nuevo.");
				}
		}
	}
}