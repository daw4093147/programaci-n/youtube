package youtube;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Video {
	static Scanner sc = new Scanner(System.in);	
	private String titulo;
	private int likes;
	private Date fechaVideo;
	ArrayList<Comentario> comentario;
	
	public Video(String titulo, int likes) {
		this.setTitulo(titulo);
		this.setLike(0);
		this.setFecha(new Date());
		this.comentario = new ArrayList<>();
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getLike() {
		return likes;
	}
	
	public void setLike(int likes) {
		this.likes = likes;
	}
	public String getFecha() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		return sdf.format(fechaVideo);
	}
	
	public void setFecha(Date fecha) {
		this.fechaVideo = fecha;
	}
	
	public void menuVideo() {
		while (true) {
			System.out.println("---" + getTitulo() + "---|");
			System.out.println("1- Nuevo comentario");
			System.out.println("2- Like");
			System.out.println("3- Mostrar comentarios");
			System.out.println("4- Mostrar información del vídeo");
			System.out.println("0- Salir");
			System.out.println("|-------------|");
			
			int opcion = sc.nextInt();
			sc.nextLine();
			
				switch(opcion) {
					case 0:
						 return;
					case 1:
						nuevoComentario();
						break;
					case 2:
						like();
						break;
					case 3:
						mostrarComentarios();
						break;
					case 4:
						mostrarInfoVideo();
						break;
					default:
						System.out.println("Opción no válida. Inténtalo de nuevo.");
				}
		}		
	}

	
	public void nuevoComentario() {
		System.out.println("Introduce un nombre de usuario");
		String usuario = sc.nextLine();
		System.out.println("Introduce un comentario");
		String comentarios = sc.nextLine();
		
		Comentario nuevoComentario = new Comentario(usuario, comentarios);
		comentario.add(nuevoComentario);
		System.out.println("");
		
	}
	
	public void like() {
		likes++;
		System.out.println("¡Has dado a like!");
	}
	
	public void mostrarComentarios() {
		if (comentario.isEmpty()) {
			System.out.println("No hay comentarios en tu vídeo");
		} else {
			for (Comentario comentarios : comentario) {
				System.out.println("Comentario: " + comentarios.getComentario() + " del usuario: " +
				comentarios.getUsuario() + " en fecha: " + comentarios.getFecha());
			}
		}
		
	}
	
	public void mostrarInfoVideo() {
			System.out.println("Video: " + getTitulo() + " en fecha: " + getFecha() + 
					" con " + likes + " likes y " + comentario.size() + " comentarios");
	}
}
