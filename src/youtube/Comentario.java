package youtube;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Comentario {
	
	private String comentario;
	private String usuario;
	private Date fechaComentario;
	
	public Comentario(String usuario, String comentario) {
		this.setComentario(comentario);
		this.setUsuario(usuario);
		this.setFecha(new Date());
	}
	
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getFecha() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		return sdf.format(fechaComentario);
	}
	public void setFecha(Date fecha) {
		this.fechaComentario = fecha;
	}
}
