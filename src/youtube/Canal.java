package youtube;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Canal {
	
	static Scanner sc = new Scanner(System.in);
	private String nombreCanal;
	private Date fechaCanal;
	ArrayList<Video> videos;
	
	public Canal(String nombre) {
		this.setNombre(nombre);
		this.setFecha(new Date());
		this.videos = new ArrayList<>();
	}

	public String getNombre() {
		return nombreCanal;
	}
	
	public void setNombre(String nombre) {
		this.nombreCanal = nombre;
	}
	
	public String getFecha() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		return sdf.format(fechaCanal);
	}
	
	public void setFecha(Date fechaCanal) {
		this.fechaCanal = fechaCanal;
	}
	
	public void menuCanal() {
		while (true) {
			System.out.println("---" + getNombre() + "---|");
			System.out.println("1- Nuevo vídeo");
			System.out.println("2- Selecciona Vídeo");
			System.out.println("3- Mostrar estadísticas");
			System.out.println("4- Mostrar información vídeos");
			System.out.println("0- Salir");
			System.out.println("|-------------|");
			
			int opcion = sc.nextInt();
			sc.nextLine();
			
				switch(opcion) {
					case 0:
						return;
					case 1:
						nuevoVideo();
						break;
					case 2:
						seleccionarVideo();
						break;
					case 3:
						mostrarEstadisticas();
						break;
					case 4:
						mostrarInfoVideos();
						break;
					default:
						System.out.println("Opción no válida. Inténtalo de nuevo.");
				}
		}
	} 
	
	public void nuevoVideo() {
		System.out.println("Dime el título de tu vídeo");
		String titulo = sc.nextLine();		
		
		Video nuevoVideo = new Video(titulo, 0);
		videos.add(nuevoVideo);		
		System.out.println("");
		
		nuevoVideo.menuVideo();;
	}
	
	public void seleccionarVideo() {
		Video video;
		if (videos.isEmpty()) {
			System.out.println("No hay vídeos creados");
			nuevoVideo();
		} else {
			System.out.println("Selecciona video:");
			for (int i = 0; i < videos.size(); i++) {
				video = videos.get(i);
				System.out.println(i + "-Video: " + video.getTitulo() + " creado en fecha: "
						+ video.getFecha() + " con " + video.getLike()+ " likes y " + video.comentario.size() + " comentarios.");
			}
			
			int opcion = sc.nextInt();
			sc.nextLine();

			if (opcion >= 0 && opcion < videos.size()) {
				video = videos.get(opcion);
				video.menuVideo();
			} else {
				System.out.println("Opción no válida. Intentálo de nuevo.");
			}
		}
	}
	
	public void mostrarEstadisticas() {
		System.out.println("Nombre del canal: " + getNombre() + " creado en fecha: " + getFecha() 
		+ " con " + videos.size() + " vídeos." );
	}
	
	public void mostrarInfoVideos() {
		for (Video video : videos) {
			System.out.println("Video: " + video.getTitulo() + " en fecha: " + video.getFecha() 
			+ " con " + video.getLike() + " likes y " + video.comentario.size() + " comentarios.");
		}
	}
}
